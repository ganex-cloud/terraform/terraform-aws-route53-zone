module "route53-domain-com-br-zone" {
  source = "git::https://gitlab.com/ganex-cloud/terraform/terraform-aws-route53-zone.git?ref=master"
  domain = "domain.com.br"
}
